import xml.etree.ElementTree as ET
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np

# Parse XML
def parse_xml(file_path):
    ns = {'graphml': 'http://graphml.graphdrawing.org/xmlns'}
    tree = ET.parse(file_path)
    root = tree.getroot()
    
    nodes = {}
    edges = []
    
    for node in root.findall('.//graphml:node', ns):
        node_id = int(node.get('id'))
        x = float(node.find('graphml:data[@key="x"]', ns).text)
        y = float(node.find('graphml:data[@key="y"]', ns).text)
        nodes[node_id] = (x, y)
    
    for edge in root.findall('.//graphml:edge', ns):
        source = int(edge.get('source'))
        target = int(edge.get('target'))
        edges.append((source, target))
    
    return nodes, edges

# Load the graph data from XML
node_positions, edges = parse_xml('airlines.xml')

# Create graph
G = nx.Graph()
for node_id, pos in node_positions.items():
    G.add_node(node_id, pos=pos)
G.add_edges_from(edges)

# Interpolate points along an edge
def interpolate_points(p1, p2, num_points):
    return [(p1[0] + i * (p2[0] - p1[0]) / num_points, p1[1] + i * (p2[1] - p1[1]) / num_points) for i in range(num_points + 1)]

# Initialize control points for each edge
num_control_points = 3
edge_control_points = {}
for edge in G.edges():
    p1 = node_positions[edge[0]]
    p2 = node_positions[edge[1]]
    edge_control_points[edge] = interpolate_points(p1, p2, num_control_points)

# Function to apply force-directed bundling
def apply_forces(edge_control_points, iterations, k, gravity=0.1):
    for _ in range(iterations):
        # Apply edge attraction forces
        for edge, points in edge_control_points.items():
            p1 = node_positions[edge[0]]
            p2 = node_positions[edge[1]]
            points[0] = p1
            points[-1] = p2
            for i in range(1, len(points) - 1):
                points[i] = (points[i][0] + k * (p1[0] + (i / (len(points) - 1)) * (p2[0] - p1[0]) - points[i][0]),
                             points[i][1] + k * (p1[1] + (i / (len(points) - 1)) * (p2[1] - p1[1]) - points[i][1]))

        # Apply edge repulsion forces
        for edge1, points1 in edge_control_points.items():
            for edge2, points2 in edge_control_points.items():
                if edge1 != edge2:
                    for i in range(1, len(points1) - 1):
                        for j in range(1, len(points2) - 1):
                            if points1[i] != points2[j]:
                                dx = points1[i][0] - points2[j][0]
                                dy = points1[i][1] - points2[j][1]
                                distance = np.sqrt(dx**2 + dy**2)
                                if distance > 0:
                                    force = k / distance
                                    points1[i] = (points1[i][0] + force * dx / distance,
                                                  points1[i][1] + force * dy / distance)
                                    points2[j] = (points2[j][0] - force * dx / distance,
                                                  points2[j][1] - force * dy / distance)

        # Apply gravity to control points towards the original straight line
        for edge, points in edge_control_points.items():
            p1 = node_positions[edge[0]]
            p2 = node_positions[edge[1]]
            for i in range(1, len(points) - 1):
                points[i] = (points[i][0] + gravity * (p1[0] + (i / (len(points) - 1)) * (p2[0] - p1[0]) - points[i][0]),
                             points[i][1] + gravity * (p1[1] + (i / (len(points) - 1)) * (p2[1] - p1[1]) - points[i][1]))

# Apply the bundling forces
apply_forces(edge_control_points, iterations=2, k=0.1)

with open('iteration2.txt','w') as f:
    f.write(str(edge_control_points))

apply_forces(edge_control_points, iterations=3, k=0.1)

with open('iteration5.txt','w') as f:
    f.write(str(edge_control_points))

# Plotting the bundled edges and nodes using matplotlib
plt.figure(figsize=(8, 6))

# Draw nodes
nx.draw_networkx_nodes(G, pos=node_positions, node_size=300, node_color='lightblue', edgecolors='k')

# Draw bundled edges
for edge, points in edge_control_points.items():
    points = np.array(points)
    print(points)
    plt.plot(points[:, 0], points[:, 1], color='gray', alpha=0.6)

# Draw node labels
nx.draw_networkx_labels(G, pos=node_positions, font_size=12, font_color='black')

plt.axis('off')
plt.show()
