
var width = 1600;
var height = 700;
var svg = d3.select("body").append("svg")
.attr("width", width)
.attr("height", height)
.attr("viewBox", [0, 0, width, height])
.attr("background-image", url)
.style("border", "1px solid black")
.on("click", deselected);


var g = svg.append("g");

// Load the GraphML (XML)
var xhr = new XMLHttpRequest();
xhr.open('GET', 'http://localhost:63342/airlines.xml', true);

xhr.timeout = 2000; // time in milliseconds

xhr.onload = function () {
  // Request finished. Do processing here.
    var xmlDoc = this.responseXML; // <- Here's your XML file
    draw_data(xmlDoc)
};

xhr.ontimeout = function (e) {
  // XMLHttpRequest timed out. Do something here.
};

xhr.send(null);


//____________________________FUNCTIONS________________________________________
function deselected(event, d) {
    d3.selectAll("circle").attr("stroke", "black");
    event.stopPropagation();
}

function selected(event, d) {
    deselected(event, d);
    event.srcElement.setAttribute("stroke", "red");
}

function zoomed({transform}) {
    g.attr("transform", transform);
}

function draw_data(xml_file){
    // parse nodes
    data = []
    nodes = xml_file.getElementsByTagName("node")
    for (let i = 0; i < nodes.length; i++) {
        const elements = nodes[i].getElementsByTagName("data")
        x_val = elements[0].innerHTML
        tooltip = elements[1].innerHTML
        y_val = elements[2].innerHTML
        data[i] = { x: Number(x_val), y: Number(y_val), r: 10, c: Math.floor(Math.random()*3)}
    }
    var color = ["yellow", "green", "blue"];
    g.selectAll("circle")
    .data(data)
    .enter().append("circle")
    .attr("id", (d, i) => "circle" + i)
    .attr("cx", d => d.x)
    .attr("cy", d => d.y)
    .attr("r", d => d.r)
    .attr("fill", d => color[d.c])
    .attr("stroke", "black")
    .on("click", selected); //Here we are adding selection

    var minX;
    var maxX;
    var minY;
    var maxY;
    var first = true;
    for(circle of g.selectAll("circle")) {
        var cx = Number(circle.getAttribute("cx"));
        var cy = Number(circle.getAttribute("cy"));
        var r = Number(circle.getAttribute("r"));
        if(first) {
            first = false;
            minX = cx - r;
            maxX = cx + r;
            minY = cy - r;
            maxY = cy + r;
        }
        else {
            minX = Math.min(minX, cx - r);
            maxX = Math.max(maxX, cx + r);
            minY = Math.min(minY, cy - r);
            maxY = Math.max(maxY, cy + r);
        }
    }
    var sceneWidth = maxX - minX;
    var sceneHeight = maxY - minY;
    var scale = Math.min(width/sceneWidth, height/sceneHeight);

    var zoomExtent = d3.zoom().scaleExtent([0.1, 10]).on("zoom", zoomed);
    svg.call(zoomExtent);
    svg.call(zoomExtent.transform, d3.zoomIdentity.scale(scale).translate(-minX, -minY));
}