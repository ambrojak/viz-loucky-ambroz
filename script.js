
var map_filename = "us.png";
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var xmlDoc; // Variable to hold the XML data
var bundling_output; // Variable to hold the XML data

var bundling_active = true;

let canvas_scale = 1.0; //canvas scale
let originX = 0; //relative origin x
let originY = 0; //relative origin y
var offset = 75; //default offset from the edges
var min_x = 0; //initial scale variable
var min_y = 0; //initial scale variable
var scale_x; //total current graph data scale x
var scale_y; //total current graph data scale y

// Data arrays
var circles = []; // Array to store circle data - hover
var data = []; // Array to store airports
var edge_data = []; // Array to store edges

//circle style
var small_radius = 2;
var medium_radius = 5;
var big_radius = 7;
var biggest_radius = 12;
var circle_stroke = 1;
var circle_color = 'rgba(20, 20, 100, 1)';


// Handle pan
let isDragging = false;
let lastX, lastY;

// Load and display background image
var img = new Image();
img.src = map_filename;
img.onload = function () {
    console.log("Loaded Image");
    console.log("Image natural width:", img.naturalWidth);
    console.log("Image natural height:", img.naturalHeight);
    console.log("Canvas width:", canvas.width);
    console.log("Canvas height:", canvas.height);

    // Once both image and XML data are loaded, call draw_data
    if (xmlDoc) {
        draw_data(xmlDoc);
    }
}
img.onerror = function () {
    console.error("Error loading image");
}

// Load and display graph data
var xhr = new XMLHttpRequest();
xhr.open('GET', './airlines.xml', true);
xhr.timeout = 2000; // time in milliseconds

xhr.onload = function () {
    // Request finished. Do processing here.
    xmlDoc = this.responseXML; // Save XML data to the variable
    console.log("XML data loaded successfully:", xmlDoc); // Print XML data to console

    var nodes = xmlDoc.getElementsByTagName("node");
    var edges = xmlDoc.getElementsByTagName("edge");
    console.log(nodes);
    console.log(edges)

    const MyNodes = [];
    const MyEdges = [];

    // Extract nodes
    for (let i = 0; i < nodes.length; i++) {
        const elements = nodes[i].getElementsByTagName("data");
        var latitude = parseFloat(elements[0].innerHTML);
        var longitude = parseFloat(elements[2].innerHTML);
        MyNodes.push({id : i, x:latitude,y:longitude})
    }
    console.log(MyNodes)

    for (let i = 0; i < edges.length; i++) {
        var edge = edges[i];
        var src = edge.getAttribute("source");
        var tgt = edge.getAttribute("target");
        MyEdges.push({id:i,source:src,target:tgt})
    }
    console.log(MyEdges)

    // use edge bundling from: https://github.com/upphiminn/d3.ForceBundle
    var fbundling = d3.ForceEdgeBundling().nodes(MyNodes).edges(MyEdges);
    bundling_output = fbundling();
    console.log(bundling_output);

    // Once both image and XML data are loaded, call draw_data
    if (img.complete) {
        draw_data(xmlDoc);
    }
};

xhr.ontimeout = function (e) {
    console.error("XML data not loaded: Timeout");
};

xhr.onerror = function () {
    console.error("Error loading XML data.");
};

xhr.send(null);

// Handle zoom
canvas.addEventListener('wheel', (event) => {
    event.preventDefault();
    const zoomFactor = 1.1;
    const mouseX = event.offsetX;
    const mouseY = event.offsetY;

    if (event.deltaY < 0) {
        // Zoom in
        canvas_scale *= zoomFactor;
        originX = mouseX - zoomFactor * (mouseX - originX);
        originY = mouseY - zoomFactor * (mouseY - originY);
    } else {
        // Zoom out
        canvas_scale /= zoomFactor;
        originX = mouseX - (mouseX - originX) / zoomFactor;
        originY = mouseY - (mouseY - originY) / zoomFactor;
    }

    draw_data(xmlDoc);
});

canvas.addEventListener('mousedown', (event) => {
    isDragging = true;
    lastX = event.offsetX;
    lastY = event.offsetY;
});

canvas.addEventListener('mousemove', (event) => {
    if (isDragging) {
        const dx = event.offsetX - lastX;
        const dy = event.offsetY - lastY;
        originX += dx;
        originY += dy;
        lastX = event.offsetX;
        lastY = event.offsetY;
        draw_data(xmlDoc);
    }
});

canvas.addEventListener('mouseup', () => {
    isDragging = false;
});

canvas.addEventListener('mouseout', () => {
    isDragging = false;
});


// Function to draw data
function draw_data(xml_file) {
    ctx.save(); // Save the current state
    ctx.clearRect(0, 0, canvas.width, canvas.height); // Clear the canvas

    // Apply transformations
    ctx.translate(originX, originY);
    ctx.scale(canvas_scale, canvas_scale);

    ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

    // Parse nodes
    if (xml_file === undefined) {
        console.error("xml_file is undefined.");
        return;
    }

    var nodes = xml_file.getElementsByTagName("node");
    var edges = xml_file.getElementsByTagName("edge");
    data = [];
    edge_data = [];
    min_x = 0;
    min_y = 0;

    for (let i = 0; i < nodes.length; i++) {
        const elements = nodes[i].getElementsByTagName("data");
        var latitude = parseFloat(elements[0].innerHTML);
        var longitude = parseFloat(elements[2].innerHTML);
        var coords = { x: latitude, y: longitude };
        if (coords.x < min_x) min_x = coords.x;
        if (coords.y < min_y) min_y = coords.y;
        data.push({ x: coords.x, y: coords.y, r: 10, c: Math.floor(Math.random() * 3), t: elements[1].innerHTML.substring(0, 3) });
    }
    scale_x = (canvas.width - 2 * offset) / (Math.max.apply(null, data.map(d => d.x)) - min_x);
    scale_y = (canvas.height - 2 * offset) / (Math.max.apply(null, data.map(d => d.y)) - min_y);

    circles = []; // Clear previous circles data

    // Draw circles for each data point
    for (let i = 0; i < data.length; i++) {
        var tag = data[i].t;
        var x = (data[i].x - min_x) * scale_x + offset;
        var y = (data[i].y - min_y) * scale_y + offset;

        // Store circle data
        circles.push({ x: x, y: y, radius: 0, tag: tag, ec: 0 });
    }

    //draw edges as as straight lines
    for (let i = 0; i < edges.length; i++) {
        var edge = edges[i];
        var source = edge.getAttribute("source");
        var target = edge.getAttribute("target");
        circles[source].ec += 1;
        circles[target].ec += 1;
        edge_data.push({ s: source, t: target, w: 2 });
        if(!bundling_active)
        {
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.strokeStyle = 'rgba(100, 100, 255, 0.2)';
            ctx.moveTo((data[source].x - min_x) * scale_x + offset, (data[source].y - min_y) * scale_y + offset);
            ctx.lineTo((data[target].x - min_x) * scale_x + offset, (data[target].y - min_y) * scale_y + offset);
            ctx.stroke();
        }
    }
    if(bundling_active){
    // Draw bundled edges
        for (let i = 0; i < bundling_output.length; i++) {
            const edge = bundling_output[i];
            ctx.beginPath();
            ctx.lineWidth = 1;
            ctx.strokeStyle = 'rgba(100, 100, 255, 0.2)';
            ctx.moveTo((edge[0].x - min_x) * scale_x + offset, (edge[0].y - min_y) * scale_y + offset);
            for (let j = 1; j < edge.length; j++) {
                const edge_part = edge[j];
                ctx.lineTo((edge_part.x - min_x) * scale_x + offset, (edge_part.y - min_y) * scale_y + offset);
            }
            ctx.stroke()
        }
    }
    for (let i = 0; i < circles.length; i++) {
        var rad = small_radius;
        circles[i].radius = medium_radius;
        if(circles[i].ec > 2 && circles[i].ec < 32)
        {
            rad = medium_radius;
            circles[i].radius = medium_radius;
        }
        else if(circles[i].ec >= 32 && circles[i].ec < 100)
        {
            rad = big_radius;
            circles[i].radius = big_radius;
        }
        else if(circles[i].ec >= 100)
        {
            rad = biggest_radius;
            circles[i].radius = biggest_radius;
        }

        // Draw circle
        ctx.beginPath();
        ctx.arc(circles[i].x, circles[i].y, rad, 0, 2 * Math.PI);
        ctx.fillStyle = circle_color;
        ctx.fill();
        ctx.lineWidth = circle_stroke;
        ctx.strokeStyle = "black";
        ctx.stroke();
    }

    ctx.restore();
}

// Function to check if the mouse is over a circle
function isMouseInCircle(mouseX, mouseY, circle) {
    var dx = mouseX - circle.x;
    var dy = mouseY - circle.y;
    return (dx * dx + dy * dy) <= (circle.radius * circle.radius);
}

// Handle hover
canvas.addEventListener('mousemove', (event) => {
    // Adjust mouse coordinates for transformations
    var rect = canvas.getBoundingClientRect();
    var mouseX = (event.clientX - rect.left - originX) / canvas_scale;
    var mouseY = (event.clientY - rect.top - originY) / canvas_scale;
    var hovered = false;

    for (let i = 0; i < circles.length; i++) {
        if (isMouseInCircle(mouseX, mouseY, circles[i])) {
            hovered = true;
            draw_data(xmlDoc);
            ctx.save();
            ctx.translate(originX, originY);
            ctx.scale(canvas_scale, canvas_scale);

            if(!bundling_active)
            {
                for (let j = 0; j < edge_data.length; j++) {
                    var edge = edge_data[j];
                    var incident = false;
                    var isSource = false;
                    if (edge.s == i) {
                        incident = true;
                        isSource = true;
                    }
                    if (edge.t == i) {
                        incident = true;
                    }
                    if(incident)
                    {
                        ctx.beginPath();
                        ctx.moveTo((data[edge.s].x - min_x) * scale_x + offset, (data[edge.s].y - min_y) * scale_y + offset);
                        ctx.lineTo((data[edge.t].x - min_x) * scale_x + offset, (data[edge.t].y - min_y) * scale_y + offset);
                        ctx.lineWidth = 2;
                        ctx.strokeStyle = 'rgba(255,30,30,0.8)';
                        ctx.stroke();
                        // if(isSource)
                        // {
                        //     ctx.font = "12px Arial";
                        //     ctx.fillText(circles[edge.t].tag, circles[edge.t].x, circles[edge.t].y - 12);
                        // }
                        // else
                        // {
                        //     ctx.font = "12px Arial";
                        //     ctx.fillText(circles[edge.s].tag, circles[edge.s].x, circles[edge.s].y - 12);
                        // }
                    }
                }
            }

            if(bundling_active)
            {
                for (let j = 0; j < bundling_output.length; j++) {
                    const edge = bundling_output[j];
                    let last = edge[edge.length -1];
                    if (edge[0].id == i) {
                        ctx.beginPath();
                        ctx.lineWidth = 2;
                        ctx.strokeStyle = 'rgba(255,30,30,0.8)';
                        ctx.moveTo((edge[0].x - min_x) * scale_x + offset, (edge[0].y - min_y) * scale_y + offset);
                        for (let k = 0; k < edge.length; k++) {
                            const edge_part = edge[k];
                            ctx.lineTo((edge_part.x - min_x) * scale_x + offset, (edge_part.y - min_y) * scale_y + offset);
                        }
                        // ctx.font = "12px Arial";
                        // ctx.fillText(circles[last.id].tag, circles[last.id].x, circles[last.id].y - 12);
                        ctx.stroke()
                    }
                }
            }
            //draw circle
            ctx.beginPath();
            ctx.arc(circles[i].x, circles[i].y, circles[i].radius, 0, 2 * Math.PI);
            ctx.fillStyle = 'rgb(252,51,51)';
            ctx.lineWidth = circle_stroke;
            ctx.strokeStyle = "rgb(68,68,68)"; // Change border color on hover
            ctx.fill();
            ctx.stroke();

            // Draw tag bg
            ctx.beginPath();
            ctx.strokeStyle = "rgba(255, 255, 255, 0.6)";
            ctx.fillStyle = "rgba(255, 255, 255, 0.6)";
            ctx.roundRect(circles[i].x - 2, circles[i].y - 40, 47, 25, [2,2,2,2]);
            ctx.fill();
            ctx.stroke();

            // Draw tag
            ctx.font = "bold 20px Arial";
            ctx.fillStyle = "rgba(0, 0, 0, 1)";
            ctx.fillText(circles[i].tag, circles[i].x, circles[i].y - 20);

            ctx.restore();

            break;
        }
    }

    if (!hovered) {
        draw_data(xmlDoc); // Redraw the canvas if not hovering over any circle
    }
});

canvas.addEventListener('contextmenu', () => {
    event.preventDefault();
    bundling_active = !bundling_active;
});
