# Napady
 - asi JS
 - Proste zobrazit ty body do prostoru
 - cesty mezi nimi
 - transormace z latitude altitude do projekce mapy
 - Pridat pod to mapu realistickou USA
 - pri zakliknuti letiste zvyraznit lety tam a pryc
 - abstraknti zobrazeni blizkosti letist
 - ruzne velke nodes letsti pro vice letu
 - vyhledani letist podle jmena/zkratky
 - nejkratsi cesta mezi 2 letisti
 - lety ruzne druhy (rovne, nerovne, neco?) cary - aby to bylo prehledne

# Poznamky z konzultace
 - kategorie letist podle veliksoti, jako mesta na mape treba, aby velka letiste neprekrila mensi v okoli
 - edge bundling - zaoblovani krivek, shlukovani hran
 - report okolo 7-8 stran
 - prepripraveni hran, do souboru napr., protoze to je dlouhe pocitani
 - projekce podle mapy zkusit
 - vyhnout se pouzivani d3 force, udelat si vlastni simulaci

# TODOlist:
 - udelat github
 - udelat nekde hosotvani pro online verzi?
 - z kodu na jmena letsit csv najit
 
# Hodina pred STAR reportem
-  úvod do zadání a dat
- co na nich chceme udělat
- rešerše vizualizačních technik
- závěr≈ pohled do budoucna, co chceme děla

# Hodina po STAR reportu

- You should sign your name (we are ok)
- Problem with references? Probably ok, preferred Some One et. al [1]
- Lot of people forgetting description how to interact with the model
- Hezký report! Byl pochválen :D
- POzor na shlukování letišť! Jak se pak selectne letiště např - když zazoomuju, tak se mi to rozpadne na menší letiště, ale ztratím kontext, kam že to vlastně letí.
- shlukování hran si klidně předpočítat, konvergovat to bude dlouho, udělat z průběžných výsledků třeba nějaký snapshoty a pak fake animaci (jakože to nebude v reálném čase), která se zobrazí uživateli. 
- zoom a pan není tak důležitý
- možnost hledat letiště podle názvu, jména, zvýrazěnní kam to lítá. Třeba nějaký seznam nebo něco.
- vykrelosvat nejřív velký pak malý letiště, aby se nepřekrývali
- velikost letiště ne nutně lineárně se zvětšující plocha, ale modifikovat tu mapovací funkci, protože lidská percepce není úplně dobrá v tom
- edge bundling vlastnoručně, ne že to jen hodíme do D3 knihovny force? To je to klíčový v téhle vizualizaci, to si máme ošahat